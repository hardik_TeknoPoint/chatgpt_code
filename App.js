import { View, Text, Pressable, StyleSheet, TextInput } from 'react-native';
import React, { useEffect, useRef } from 'react';
// const {CalendarModule} = ReactNative.NativeModules;
// import { NativeModules } from 'react-native';
// const { CalendarModule } = NativeModules;
import MyRnNativeModuleBridge from './src/screens/rnNativeModuleBridge';
import TodoAppScreen from './src/screens/todoAppScreen';
import TakeInfoScreen from './src/screens/takeInfoScreen';
import CarouselScreen from './src/screens/carouselScreen';



const viewObj = {
  "type": {},
  "key": null,
  "ref": null,
  "props": {
    "style": {
      "backgroundColor": "red"
    },
    "children": {
      "type": {
        "propTypes": {}
      },
      "key": null,
      "ref": null,
      "props": {
        "children": "Hello"
      },
      "_owner": null,
      "_store": {}
    }
  },
  "_owner": null,
  "_store": {}
}

const Obj = JSON.stringify(<View style={{ backgroundColor: "red" }}><Text>Hello</Text></View>, null, 2);




export default function App() {
  const myRef = useRef(null);

  class MyComponent extends React.Component {
    render() {
      return (
        <View style={{ backgroundColor: "red" }} ref={myRef}>
          <Text>Hello, world!</Text>
        </View>
      );
    }
  }

  const myComponentInstance = new MyComponent();



  // useEffect(() => {
  //   // console.log("my Obj ==", Obj);
  //   // console.log("myComponentInstance==..",JSON.stringify(myComponentInstance.render(),null,5)); // outputs: {}
  //   // console.log(JSON.stringify(myComponentInstance.context, null, 5));
  // });

  return (
    <View style={{flex:1}}>
      {/* <TodoAppScreen /> */}
      {/* <MyRnNativeModuleBridge /> */}
      {/* <TakeInfoScreen /> */}
      <CarouselScreen />
    </View>
  )
}

const styles = StyleSheet.create({
  btnStyle: {
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#7722e6",
    marginLeft: "auto",
    marginRight: "auto",
    padding: 15,
    borderRadius: 10,
    marginVertical: 10
  },
  textInputStyle: {
    backgroundColor: "#c5c5c5",
    borderWidth: 2,
    borderColor: "#a5a5a5",
    borderRadius: 10,
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto",
    marginVertical: 5,
    paddingLeft: 20
  }
});