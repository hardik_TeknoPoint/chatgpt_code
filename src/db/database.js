import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase(
  {
    name: 'todoList.db',
    location: 'default',
  },
  () => console.log('Database opened'),
  error => console.log('Error opening database:', error)
);
// isCompleted: false,
// taskName: "hello"

const createTaskTable = () => {
    try {
        db.transaction(tx => {
            tx.executeSql(
                'CREATE TABLE IF NOT EXISTS Todos (ID INTEGER PRIMARY KEY AUTOINCREMENT, TaskName TEXT, IsCompleted BOOLEAN )'
                , null, (txObj, resultSet) => {
                    console.log('CreateUserTable', resultSet);
                }, (txObj, error) => {
                    console.log('User Table Creation Error : ', txObj.message);
                }
            );
        })
    }
    catch (ex) {
        console.log('Table Creation Error: ' + ex);
    }
}
createTaskTable();

export default db;