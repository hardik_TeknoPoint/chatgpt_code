import { View, Text, Pressable, StyleSheet } from 'react-native'
import React from 'react'
import Icon from '../reuseableComponents/icon';

export default function TaskLi({taskName,toggleComplated,isCompleted,deleteTodo,ID}) {
    return (
        <View style={{ padding: 10, backgroundColor: "#e5e5e5", marginVertical: 5, borderRadius: 5, paddingHorizontal: 15, flexDirection: "row" }}>
            <Text style={{ fontSize: 16, fontWeight: "400", marginRight: "auto", color: "grey" }}>{taskName}</Text>
            <Pressable style={({ pressed }) => ([{ opacity: pressed ? 0.5 : 1 }])}>
                <Icon groupName={"Feather"} iconName={"edit-3"} iconsize={25} iconstyle={styles.listBtnStyle} />
            </Pressable>
            <Pressable style={({ pressed }) => ([{ opacity: pressed ? 0.5 : 1 }])}
                onPress={() => {
                    toggleComplated(ID);
                }}
            >
                <Icon groupName={"AntDesign"} iconName={"checkcircle"} iconsize={25} iconstyle={[styles.listBtnStyle, isCompleted && styles.complatedStyle]} />
            </Pressable>
            <Pressable style={({ pressed }) => ([{ opacity: pressed ? 0.5 : 1 }])}
                onPress={() => {
                    deleteTodo(ID);
                }}
            >
                <Icon groupName={"MaterialCommunityIcons"} iconName={"delete"} iconsize={28} iconstyle={[styles.listBtnStyle, { color: "#ff876688" }]} />
            </Pressable>
        </View>
    )
}


const styles = StyleSheet.create({
    addBtn: {
        backgroundColor: "#c5c5c5",
        padding: 10,
        borderRadius: 5,
        paddingHorizontal: 15
    },
    listBtnStyle: {
        color: "#b5b5b5",
        marginHorizontal: 8
    },
    complatedStyle:{
        color:"green"
    }
})