import React, { useState } from 'react';
import { View, FlatList, StyleSheet, Dimensions, Pressable } from 'react-native';
import { heightToDp, widthToDp } from '../utils/responsive';
import VectorIcons from '../components/vectorIcons';


// const SLIDER_WIDTH = Dimensions.get('window').width;
const windowWidth = Dimensions.get('window').width;
let currentOffsetValue = 0;


let ITEM_WIDTH = widthToDp(90); // the width of your carousel item
let ITEM_MARGIN = 0; // the margin between carousel items
let CAROUSEL_WIDTH = ITEM_WIDTH + ITEM_MARGIN * 2; // the total width of the carousel item, including margin

const MyCarousel = ({ DATA, viewabilityConfig, currentItem, item_width, item_margin, carousel_width, defaultCenter, renderItemFunc }) => {

    // defaultCenter when true make render item in center of carousel.
    ITEM_WIDTH = defaultCenter ? widthToDp(90) : item_width || widthToDp(90);
    ITEM_MARGIN = defaultCenter ? 0 : item_margin || 0;
    CAROUSEL_WIDTH = defaultCenter ? ITEM_WIDTH + ITEM_MARGIN * 2 : carousel_width || ITEM_WIDTH + ITEM_MARGIN * 2;
    const [currentIndex, setCurrentIndex] = useState(0);
    let snapIntervalAmount = DATA.length > 1 ? widthToDp(80) + ITEM_MARGIN * 2 : CAROUSEL_WIDTH;

    function forWardFlatList() {
        if (currentIndex != (DATA.length - 1)) {
            console.log("forWardFlatList ==>>");
            handleScrollToOffset(currentOffsetValue += widthToDp(80));
            setCurrentIndex(preValue => preValue++);
        }
    }

    function backWardFlatList() {
        if (currentIndex != 0) {
            console.log("<<== backWardFlatList");
            handleScrollToOffset(currentOffsetValue -= widthToDp(80));
            setCurrentIndex(preValue => preValue--);
        }
    }

    function handleScrollToOffset(offsetValue) {
        viewabilityConfig.current.scrollToOffset({ offset: offsetValue, animated: true });
    };

    if (DATA.length > 1) {
        ITEM_WIDTH = widthToDp(80);
        CAROUSEL_WIDTH = widthToDp(80) + ITEM_MARGIN * 2;
    } else {
        ITEM_WIDTH = widthToDp(90);
        CAROUSEL_WIDTH = widthToDp(90) + ITEM_MARGIN * 2;
    }

      const handleScroll = (event) => {
        const { contentOffset } = event.nativeEvent;
        const index = Math.round(contentOffset.x / CAROUSEL_WIDTH);
        currentOffsetValue = index * widthToDp(80);
        setCurrentIndex(index);
      };

    return (<View style={styles.container}>
            {currentIndex != 0 && <Pressable style={({ pressed }) => ([styles.leftArrowBtn, { opacity: pressed ? 0.5 : 1 }])}
                onPress={backWardFlatList}>
                <VectorIcons groupName="AntDesign" iconName="caretleft" iconsize={widthToDp(5)} iconstyle={styles.ArrowIcon} />
            </Pressable>}
            {(DATA.length - 1) != currentIndex && <Pressable style={({ pressed }) => ([styles.rightArrowBtn, { opacity: pressed ? 0.5 : 1 }])}
                onPress={forWardFlatList}>
                <VectorIcons groupName="AntDesign" iconName="caretright" iconsize={widthToDp(5)} iconstyle={styles.ArrowIcon} />
            </Pressable>}
            <FlatList
                data={DATA}
                ref={viewabilityConfig}
                horizontal
                snapToInterval={snapIntervalAmount}
                decelerationRate={"fast"}
                onScroll={handleScroll}
                contentContainerStyle={[styles.contentContainer, currentIndex == 0 && { paddingLeft: -widthToDp(5.5) }]}
                keyExtractor={(item, index) => index}
                renderItem={({ item, index })=>(
                    <View style={[styles.item, { width: DATA.length > 1 ? widthToDp(80) : widthToDp(90), }]}>
                        <View style={{ margin: 0, padding: 0, width: windowWidth - widthToDp(DATA.length == 1 ? 14 : 20) }}>
                            {renderItemFunc(item,index)}
                        </View>
                    </View>
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: "visible",
        zIndex: -1
    },
    contentContainer: {
        paddingHorizontal: ITEM_MARGIN,
        margin: 0,
        borderWidth: 2,
        borderColor: "black",
        paddingLeft: widthToDp(5.5)
    },
    item: {
        width: ITEM_WIDTH,
    },
    leftArrowBtn: {
        position: "absolute",
        zIndex: 10,
        left: 0,
        paddingVertical: widthToDp(10),
        paddingHorizontal: widthToDp(1),
        backgroundColor: "#52525255",
        borderRadius: 5
    },
    rightArrowBtn: {
        position: "absolute",
        zIndex: 10,
        right: 0,
        paddingVertical: widthToDp(10),
        paddingHorizontal: widthToDp(1),
        backgroundColor: "#52525255",
        borderRadius: 5
    },
    ArrowIcon: {
        color: "grey"
    },
});

export default MyCarousel;