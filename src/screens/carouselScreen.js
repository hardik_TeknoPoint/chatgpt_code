import { View, Text, ScrollView } from 'react-native';
import React, { useRef } from 'react';
import MyCarousel from '../components/myCarousel';
import { heightToDp, widthToDp } from '../utils/responsive';

export default function CarouselScreen() {
    const dataArray = [
        { name: "name-1" },
        { name: "name-2" },
        { name: "name-3" },
        { name: "name-4" },
        { name: "name-5" },
    ];
    const viewabilityConfig = useRef(null);

    // const handleScroll = (event) => {
    //     const { contentOffset } = event.nativeEvent;
    //     const index = Math.round(contentOffset.x / CAROUSEL_WIDTH);
    //     currentOffsetValue = index * widthToDp(80);
    //     setCurrentIndex(index);
    // };

    return (
        <View style={{ backgroundColor: "#25252525", width: widthToDp(100),paddingHorizontal:widthToDp(3) }}>
            <Text>CarouselScreen</Text>
            <ScrollView>
                <MyCarousel
                    DATA={dataArray}
                    viewabilityConfig={viewabilityConfig}
                    renderItemFunc={(item, index) => {
                        console.log("from renderItem carouselScreen.");
                        return (
                            <View style={{  backgroundColor: "#25252525", height: heightToDp(30),justifyContent:"center" }}>
                                <Text style={{backgroundColor:"blue",borderWidth:1,textAlign:"center",height:heightToDp(20)}}>{item.name}</Text>
                            </View>
                        )
                    }}
                />
            </ScrollView>
        </View>
    )
}