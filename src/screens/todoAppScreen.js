import { View, Text, StyleSheet, TextInput, Pressable, FlatList, ScrollView, SafeAreaView } from 'react-native';
import React, { useEffect, useState } from 'react';
import TaskLi from '../components/TaskLi';
// import db from '../db/database';
import DB from "../db/database";


export default function TodoAppScreen() {

    const [taskList, setTaskList] = useState([
        // {
        //     isCompleted: false,
        //     taskName: "hello"
        // },
    ]);
    const [taskName, setTaskName] = useState("");



    const addTodo = (taskText) => {
        DB.transaction((tx) => {
            tx.executeSql(
                // 'INSERT INTO todos (TaskName,IsCompleted) VALUES ("'+taskText+'","'+false+'")',
                'INSERT INTO todos (TaskName,IsCompleted) VALUES (?,?)',
                [taskText,false],
                (_, result) => {
                    console.log('Todo added with ID:', result.insertId);
                },
                (_, error) => {
                    console.log('Error adding todo:', error);
                }
            );
        });
        getTaskList();
    };

    function getTaskList() {
        DB.transaction(tx => {
            tx.executeSql(
                'SELECT * FROM Todos',
                [],
                (_, { rows }) => {
                    console.log("rows from DB ===>>>>", rows.raw());
                    setTaskList(rows.raw()),
                        error => console.log('Error retrieving todos:', error)
                }
            );
        });
    }

    useEffect(() => {
        getTaskList();
        console.log("taskList==>>", taskList);
    }, []);

    function addToList() {
        addTodo(taskName);
        // setTaskList((pre) => [...pre, {
        //     isCompleted: false,
        //     taskName: taskName
        // },]);
        setTaskName("");
    }

    const deleteTodo = (id) => {
        DB.transaction((tx) => {
          tx.executeSql(
            'DELETE FROM todos WHERE id = ?',
            [id],
            (_, result) => {
              console.log(`Deleted ${result.rowsAffected} todo`);
            },
            (_, error) => {
              console.log('Error deleting todo:', error);
            }
          );
        });
        getTaskList();
      };
    function toggleComplated(ID) {
        const newTaskList = taskList.map((values, i) => {
            if (ID == values.ID) {
                return { ...values, IsCompleted: !values.IsCompleted }
            }
            return values;
        });
        setTaskList(newTaskList);
    }

    function deleteTask(index) {
        const newTaskList = taskList.filter((values, i) => index != i);
        setTaskList(newTaskList);

    }
    return (
        <SafeAreaView>
            <View style={styles.todoAppContainer}>
                <Text style={{ fontSize: 20, fontWeight: "600", textAlign: "center" }}>Todo App</Text>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginVertical: 15 }}>
                    <TextInput
                        placeholder='Add Task'
                        value={taskName}
                        onChangeText={(text) => {
                            console.log("text value", text);
                            setTaskName(text);
                        }}
                        style={styles.inputText}
                    />
                    <Pressable style={({ pressed }) => ([styles.addBtn, { opacity: pressed ? 0.5 : 1 }])}
                        onPress={() => {
                            if (taskName.trim() == "") {
                                setTaskName("");
                                return;
                            };
                            addToList();
                        }}
                    >
                        <Text>Add Task</Text>
                    </Pressable>
                </View>
                <View >
                    <FlatList
                        data={taskList}
                        renderItem={({ item, index }) => {
                            console.log("item from flatlist ==>>",item);
                            return (
                                <TaskLi toggleComplated={toggleComplated} taskName={item.TaskName} isCompleted={item.IsCompleted} deleteTodo={deleteTodo} ID={item.ID} />
                            )
                        }}
                    />

                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    todoAppContainer: {
        padding: 15,
    },
    inputText: {
        padding: 5,
        paddingHorizontal: 20,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: "#e5e5e5",
        width: "73%"
    },
    addBtn: {
        backgroundColor: "#c5c5c5",
        padding: 10,
        borderRadius: 5,
        paddingHorizontal: 15
    },
})