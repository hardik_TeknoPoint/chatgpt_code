import { View, Text, Pressable, StyleSheet, TextInput, NativeModules , requireNativeComponent } from 'react-native';
import React, { useState } from 'react';
// const {CalendarModule} = ReactNative.NativeModules;
const { CalendarModule, OpenActivityModule } = NativeModules;



// const MyView = requireNativeComponent("MyCustomView");
// const MyView = requireNativeComponent("RCTImageView");

// const NewView = ()=>{
//   return <MyView/>;
// }
export default function MyRnNativeModuleBridge() {

  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  function callBackFunction(value) {
    console.log("value from callBackFunction .............", value);
  }

  function cbForError(error){
    console.log("error from callback ...............................");
  }

  async function promiseFunction() {
    try {
      const returnValue = await CalendarModule.promiseFunction('testName', 'testLocation');
      console.log("returnValue from promise ", returnValue);
    } catch (error) {
      console.log("error from promise cathc block", error);
    }
  }

  function getFullNameFunc(fullName) {
    console.log("fullName....", fullName);
  }

  return (
    <View style={{}}>
      <Text style={{textAlign:"center",fontSize:25,padding:15}}>native Module functions</Text>
      <View>

      {/* <NewView/> */}
        <TextInput
          style={[styles.textInputStyle,{marginTop:50}]}
          placeholder='Fisrt Name'
          value={fname}
          onChangeText={(text) => {
            console.log("fname text value==..", text);
            setFname(text);
          }}
        />
        <TextInput
          style={styles.textInputStyle}
          placeholder='Last Name'
          value={lname}
          onChangeText={(text) => {
            console.log("lname text value==..", text);
            setLname(text);
          }}
        />
        <Pressable
          onPress={() => {
            console.log("from submit function !!");
            // console.log("OpenActivityModule",OpenActivityModule,CalendarModule);
            OpenActivityModule.openNewScreen();
            // CalendarModule.getFullName(fname, lname, getFullNameFunc,cbForError);
            // CalendarModule.getFullName("hardik", "mistry", getFullNameFunc,cbForError);
          }}
          style={({ pressed }) => ([styles.btnStyle, { opacity: pressed ? 0.5 : 1 }])}
        >
          <Text style={{ fontSize: 16, fontWeight: "500", color: "#f5f5f5" }}>Submit</Text>
        </Pressable>
        
        <Pressable
          onPress={() => {
            CalendarModule.createCalendarEvent('testName', 'testLocation', callBackFunction);
          }}
          style={({ pressed }) => ([styles.btnStyle, { opacity: pressed ? 0.5 : 1 }])}
        >
          <Text style={{ fontSize: 16, fontWeight: "500", color: "#f5f5f5" }}>callBackFunction</Text>
        </Pressable>
        <Pressable
          onPress={promiseFunction}
          style={({ pressed }) => ([styles.btnStyle, { opacity: pressed ? 0.5 : 1 }])}
        >
          <Text style={{ fontSize: 16, fontWeight: "500", color: "#f5f5f5" }}>promiseFunction</Text>
        </Pressable>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  btnStyle: {
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#7722e6",
    marginLeft: "auto",
    marginRight: "auto",
    padding: 15,
    borderRadius: 10,
    marginVertical: 10
  },
  textInputStyle: {
    backgroundColor:"#c5c5c5",
    borderWidth:2,
    borderColor:"#a5a5a5",
    borderRadius:10,
    width:"90%",
    marginLeft:"auto",
    marginRight:"auto",
    marginVertical:5,
    paddingLeft:20
  }
});



// write simple star function in javascript.