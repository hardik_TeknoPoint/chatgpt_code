import { View, Text, StyleSheet, Pressable, NativeModules, Image } from 'react-native';
import React, { useState } from 'react';

const { myPhoneInfo, GetPhoneInfo, ImagePicker, takeCameraPicture, MyCameraModule } = NativeModules;


let functionName = "";
function cb(value) {
    console.log(functionName, "------>>", value);
}

export default function TakeInfoScreen() {

    const [imgUri, setImgUri] = useState("");
    return (
        <View>
            <Text>TakeInfoScreen</Text>

            <Pressable
                onPress={() => {
                    async function takeImage() {
                        try {
                            const imgUri = await ImagePicker.pickImageFromGallery();
                            console.log("imgUri==--->>", imgUri.uri);
                            setImgUri(imgUri.uri);
                        } catch (error) {
                            console.log("error from java==", error);
                        }
                    }
                    takeImage();
                }}
                style={({ pressed }) => ([styles.btnStyle, { opacity: pressed ? 0.5 : 1 }])}
            >
                <Text style={{ fontSize: 16, fontWeight: "500", color: "#f5f5f5" }}>Open Gallery</Text>
            </Pressable>

            <Pressable
                onPress={() => {
                    async function takeCameraImage() {
                        console.log("openCamera");
                        try {
                            const imageUri = await MyCameraModule.takePicture();
                            console.log("imageUri==--->>", imageUri);
                        } catch (error) {
                            console.log("error from java==", error);
                        }
                    }
                    takeCameraImage();
                }}
                style={({ pressed }) => ([styles.btnStyle, { opacity: pressed ? 0.5 : 1, marginTop: 20 }])}
            >
                <Text style={{ fontSize: 16, fontWeight: "500", color: "#f5f5f5" }} >Open Camera</Text>
            </Pressable>
            { imgUri &&
                <Image
                    source={{ uri: imgUri }}
                    resizeMode="contain"
                    style={{ width: 200, height: 200 }}
                />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    btnStyle: {
        width: "90%",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#7722e6",
        marginLeft: "auto",
        marginRight: "auto",
        padding: 15,
        borderRadius: 10,
        marginVertical: 10
    },
    textInputStyle: {
        backgroundColor: "#c5c5c5",
        borderWidth: 2,
        borderColor: "#a5a5a5",
        borderRadius: 10,
        width: "90%",
        marginLeft: "auto",
        marginRight: "auto",
        marginVertical: 5,
        paddingLeft: 20
    }
});