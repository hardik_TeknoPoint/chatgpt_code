package com.demo2;

import com.demo2.AndroidInfo.ImagePickerModule;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class getInfoPackage implements ReactPackage {
    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @Override
    public List<NativeModule> createNativeModules(
            ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();

//        modules.add(new CalendarModule(reactContext));
//        modules.add(new GetPhoneInfo(reactContext));
//        modules.add(new MyPhoneInfo(reactContext));
        modules.add(new ImagePickerModule(reactContext));

        return modules;
    }
}
