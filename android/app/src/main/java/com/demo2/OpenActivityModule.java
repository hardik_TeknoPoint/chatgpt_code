package com.demo2;

import android.content.Intent;

import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;


public class OpenActivityModule extends ReactContextBaseJavaModule{
    public OpenActivityModule(ReactApplicationContext reactContext){
            super(reactContext); 
    }

    @ReactMethod
    public void openNewScreen(){
        // Intent intent = new Intent(getCurrentActivity(),AndroidActivity.class);
        Intent intent = new Intent(getCurrentActivity(),CustomActivity.class);
        getCurrentActivity().startActivity(intent);
    }
 
     @Override
     public String getName(){
         return  "OpenActivityModule";
     }
}
