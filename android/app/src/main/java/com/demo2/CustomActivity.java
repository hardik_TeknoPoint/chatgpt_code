package com.demo2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;


public class CustomActivity extends  Activity{

    LinearLayout ll;
    TextView t;
    Button b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ll = new LinearLayout(this);
        t = new TextView(this);
        b = new Button(this);

        LayoutParams dimensions = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        ll.setLayoutParams(dimensions);

        LayoutParams viewDimensions = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        t.setLayoutParams(viewDimensions);
        b.setLayoutParams(viewDimensions);

        ll.setOrientation(LinearLayout.VERTICAL);
//        ll.setOrientation();
        t.setText("text view");
        b.setText("this is Button");

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("open react-n5qative", "onClick: ");


                 Intent intent = new Intent(CustomActivity.this,MainActivity.class);
                 startActivity(intent);
            }
        });


        ll.addView(t);
        ll.addView(b);

        setContentView(ll);
    }

}
