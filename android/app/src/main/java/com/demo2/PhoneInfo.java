package com.demo2;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

public class PhoneInfo {
    private TelephonyManager mTelephonyManager;
    private Context mContext;

    public PhoneInfo(Context context) {
        mContext = context;
        mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    }

    public String getDeviceId() {
        return mTelephonyManager.getDeviceId();
    }

//    public String getDeviceSoftwareVersion() {
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return TODO;
//        }
//        return mTelephonyManager.getDeviceSoftwareVersion();
//    }

//    public String getDeviceSoftwareVersion() {
//        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            // Request the missing permissions
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_READ_PHONE_STATE);
//            // The result of the request will be handled in onRequestPermissionsResult
//            return null;
//        }
//        return mTelephonyManager.getDeviceSoftwareVersion();
//    }
//
//    public String getLine1Number() {
//        if ((ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return TODO;
//        }
//        return mTelephonyManager.getLine1Number();
//    }

    public String getNetworkCountryIso() {
        return mTelephonyManager.getNetworkCountryIso();
    }

    public String getNetworkOperator() {
        return mTelephonyManager.getNetworkOperator();
    }

    public String getNetworkOperatorName() {
        return mTelephonyManager.getNetworkOperatorName();
    }

//    public int getNetworkType() {
//        return mTelephonyManager.getNetworkType();
//    }

    public int getPhoneType() {
        return mTelephonyManager.getPhoneType();
    }

    public String getSimCountryIso() {
        return mTelephonyManager.getSimCountryIso();
    }

    public String getSimOperator() {
        return mTelephonyManager.getSimOperator();
    }

    public String getSimOperatorName() {
        return mTelephonyManager.getSimOperatorName();
    }

    public String getSimSerialNumber() {
        return mTelephonyManager.getSimSerialNumber();
    }

    public int getSimState() {
        return mTelephonyManager.getSimState();
    }

    public String getSubscriberId() {
        return mTelephonyManager.getSubscriberId();
    }

//    public String getVoiceMailNumber() {
//        return mTelephonyManager.getVoiceMailNumber();
//    }

    public boolean isNetworkRoaming() {
        return mTelephonyManager.isNetworkRoaming();
    }

    public String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public String getModel() {
        return Build.MODEL;
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public int getSDKVersion() {
        return Build.VERSION.SDK_INT;
    }
}
