package com.demo2;

public class MyPromiseDemo {
    String fname = ""; 
    String lname = ""; 
    
    MyPromiseDemo(String fname, String lname ){
        this.fname = fname;
        this.lname = lname;
    }

    public String makeFullName(){
        return this.fname + "***" + this.lname;
    }
}
