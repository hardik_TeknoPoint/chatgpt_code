package com.demo2;


import android.content.Context;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class GetPhoneInfo extends ReactContextBaseJavaModule {

//    ReactApplicationContext myReactContext = null;

    private Context reactContext;
    PhoneInfo phoneInfo = new PhoneInfo(reactContext);

    GetPhoneInfo(ReactApplicationContext reactContext){
//        myReactContext = reactContext;
        super(reactContext);
    }

    @ReactMethod
    public void getDeviceId(Callback callback){
        callback.invoke(phoneInfo.getDeviceId());
    }
    @ReactMethod
    public void getNetworkCountryIso(Callback callback){
        callback.invoke(phoneInfo.getNetworkCountryIso());
    }
    @ReactMethod
    public void getNetworkOperator(Callback callback){
        callback.invoke(phoneInfo.getNetworkOperator());
    }
    @ReactMethod
    public void getNetworkOperatorName(Callback callback){
        callback.invoke(phoneInfo.getNetworkOperatorName());
    }
    @ReactMethod
    public void getPhoneType(Callback callback){
        callback.invoke(phoneInfo.getPhoneType());
    }
    @ReactMethod
    public void getSimCountryIso(Callback callback){
        callback.invoke(phoneInfo.getSimCountryIso());
    }
    @ReactMethod
    public void getSimOperator(Callback callback){
        callback.invoke(phoneInfo.getSimOperator());
    }
    @ReactMethod
    public void getSimOperatorName(Callback callback){
        callback.invoke(phoneInfo.getSimOperatorName());
    }
    @ReactMethod
    public void getSimSerialNumber(Callback callback){
        callback.invoke(phoneInfo.getSimSerialNumber());
    }
    @ReactMethod
    public void getSimState(Callback callback){
        callback.invoke(phoneInfo.getSimState());
    }
    @ReactMethod
    public void getSubscriberId(Callback callback){
        callback.invoke(phoneInfo.getSubscriberId());
    }
    @ReactMethod
    public void isNetworkRoaming(Callback callback){
        callback.invoke(phoneInfo.isNetworkRoaming());
    }
    @ReactMethod
    public void getManufacturer(Callback callback){
        callback.invoke(phoneInfo.getManufacturer());
    }
    @ReactMethod
    public void getModel(Callback callback){
        callback.invoke(phoneInfo.getModel());
    }
    @ReactMethod
    public void getOSVersion(Callback callback){
        callback.invoke(phoneInfo.getOSVersion());
    }
    @ReactMethod
    public void getSDKVersion(Callback callback){
        callback.invoke(phoneInfo.getSDKVersion());
    }
    @Override
    public String getName(){
        return "GetPhoneInfo";
    }

}
