package com.demo2.AndroidInfo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class MyPhoneInfo extends ReactContextBaseJavaModule {

    Context mContext = null;
    TelephonyManager telephonyManager;
    Activity mActivity = null;
    String phoneNumber = null;
    int PERMISSION_REQUEST_CODE = 1;
    Promise CB;
    String[] permissions = {Manifest.permission.READ_PHONE_NUMBERS};
    private static final int PERMISSION_REQUEST_READ_PHONE_STATE = 1;
    public MyPhoneInfo(ReactApplicationContext context) {
        super(context);
        mContext = context;
        mActivity = this.getCurrentActivity();
    }

    @SuppressLint("LongLogTag")
    @ReactMethod
    public void getPhoneNo(Promise callback) {
        CB = callback;
        telephonyManager = (TelephonyManager) getReactApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
//        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) {
            try {
                ActivityCompat.requestPermissions(mActivity,permissions, PERMISSION_REQUEST_CODE);
            }catch (Error err){
                Log.d("err==>>", String.valueOf(err));
            }
            Log.d("inside if----------", "getPhoneNo: ");
            return;
            // here to request the missing permissions, and then overriding
//               public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults)
        }else {
//            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//            String softwareVersion = telephonyManager.getLine1Number();
             phoneNumber = telephonyManager.getLine1Number();
        }
        Log.d("Phone_Number--", phoneNumber);
        CB.resolve("this is phone No -- ");
    }

//    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                phoneNumber = telephonyManager.getLine1Number();
                getPhoneNo(CB);
                Log.d("permission==Granted","" );
            } else {
                Log.d("permission==denied","" );
            }
        }
    }


    public String getName(){
        return "myPhoneInfo";
    }

}
