package com.demo2.AndroidInfo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.MediaStore;
import android.view.View;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.google.android.material.snackbar.Snackbar;


public class MyCameraModule extends ReactContextBaseJavaModule {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_CAMERA_PERMISSION = 2;
    private Promise mPromise;

    public MyCameraModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "MyCameraModule";
    }

    @ReactMethod
    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getReactApplicationContext().getPackageManager()) != null) {
            getCurrentActivity().startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @ReactMethod
    public void takePicture(Promise promise) {
        mPromise = promise;
        if (ContextCompat.checkSelfPermission(getReactApplicationContext(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(getCurrentActivity(), Manifest.permission.CAMERA)) {
                // Show an explanation to the user why the permission is needed
                Snackbar.make(getCurrentActivity().findViewById(android.R.id.content),
                        "Camera permission is needed to take pictures",
                        Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Request the permission
                        ActivityCompat.requestPermissions(getCurrentActivity(),
                                new String[] { Manifest.permission.CAMERA }, REQUEST_CAMERA_PERMISSION);
                    }
                }).show();
            } else {
                // Request the permission
                ActivityCompat.requestPermissions(getCurrentActivity(), new String[] { Manifest.permission.CAMERA },
                        REQUEST_CAMERA_PERMISSION);
            }
        } else {
            // Permission is already granted
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (intent.resolveActivity(getReactApplicationContext().getPackageManager()) != null) {
                getCurrentActivity().startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                mPromise.resolve(null);
            } else {
                mPromise.reject("ERROR", "Unable to open camera");
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted, take picture
                takePicture(mPromise);
            } else {
                // Permission denied, reject promise
                mPromise.reject("ERROR", "Camera permission denied");
            }
        }
    }
}
