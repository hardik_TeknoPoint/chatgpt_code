package com.demo2.AndroidInfo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.facebook.common.memory.PooledByteBufferInputStream;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class takeCameraPicture extends ReactContextBaseJavaModule {
    takeCameraPicture(ReactApplicationContext reactContext){
        super(reactContext);
    }
    @Override
    public String getName(){
        return "takeCameraPicture";
    }

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String IMAGE_TYPE = "image/*";
    private static final String FILE_PROVIDER_AUTHORITY = "com.demo2.fileprovider";
    private static final String EVENT_NAME = "onPictureTaken";
    private String mCurrentPhotoPath = null;
    public Callback mCallback = null;

    @ReactMethod
    public void takePicture( Promise promise) {
        Activity activity = getCurrentActivity();
//        if (activity == null) {
//            promise.reject("Activity is null");
//            return;
//        }

        Callback mCallback = new Callback() {
            @Override
            public void invoke(Object... args) {
                if (args.length > 0 && args[0] instanceof String) {
                    String base64String = (String) args[0];
                    WritableMap response = Arguments.createMap();
                    response.putString("base64String", base64String);
                    promise.resolve(response);
                } else {
                    promise.reject("Failed to get picture data");
                }
            }
        };

        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                File photoFile = createImageFile();
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(activity, FILE_PROVIDER_AUTHORITY, photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                } else {
                    Log.d("else-1", "takePicture: ");
                    promise.reject("Failed to create image file");
                }
            } 
            // else {
            //     // Log.d("else-2", "takePicture: ");
            //     // promise.reject("Failed to launch camera app");
            // }
        } catch (Exception e) {
            Log.d("", "takePicture: ");
            // promise.reject(e);
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = imageFile.getAbsolutePath();
        return imageFile;
    }

    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            if (mCurrentPhotoPath != null) {
                try {
                    File imageFile = new File(mCurrentPhotoPath);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int len;
                    PooledByteBufferInputStream imageFileInputStream = null;
                    while ((len = imageFileInputStream.read(buffer)) != -1) {
                        byteArrayOutputStream.write(buffer, 0, len);
                    }
                    byteArrayOutputStream.flush();
                    byte[] imageData = byteArrayOutputStream.toByteArray();
                    String base64String = Base64.encodeToString(imageData, Base64.DEFAULT);
                    sendEvent(EVENT_NAME, base64String);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
            }

    private void sendEvent(String eventName, String data) {
        ReactApplicationContext reactContext = getReactApplicationContext();
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, data);
        if (mCallback != null) {
            mCallback.invoke(data);
            mCallback = null;
        }
    }
}



