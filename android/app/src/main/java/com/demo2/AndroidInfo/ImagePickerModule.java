package com.demo2.AndroidInfo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

public class ImagePickerModule extends ReactContextBaseJavaModule implements ActivityEventListener {

    private static final int PICK_IMAGE = 1;
    private Promise mPickerPromise;
    private String mImageFilePath;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private Callback mCallback;

    public ImagePickerModule(ReactApplicationContext reactContext){
        super(reactContext);
        reactContext.addActivityEventListener(this);
    }

    @Override
    public String getName(){
        return "ImagePicker";
    }

    @ReactMethod
    public void pickImageFromGallery(Promise promise) {
        Activity activity = getCurrentActivity();
        if (activity == null) {
            promise.reject(new Exception("Activity is null"));
            return;
        }

        // Store the promise to resolve/reject when the user picks an image
        mPickerPromise = promise;

        // Create the intent to pick an image from the device's gallery
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        activity.startActivityForResult(intent, PICK_IMAGE);
    }
    @Override
    public void onActivityResult(Activity activity,int requestCode, int resultCode, Intent data){
        if (requestCode == PICK_IMAGE) {
            if (mPickerPromise != null){
                if (resultCode == Activity.RESULT_OK){
                    Uri uri = data.getData();
                    WritableMap response = new WritableNativeMap();
                    response.putString("uri", uri.toString());
                    mPickerPromise.resolve(response);
                }else {
                    mPickerPromise.reject(new Exception("User cancelled image picker"));
                }
                mPickerPromise = null;
            }
        }
    }


    // private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
    //     @Override
    //     public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
    //         if (requestCode == REQUEST_IMAGE_CAPTURE && mCallback != null) {
    //             if (resultCode == Activity.RESULT_OK) {
    //                 WritableMap result = new WritableNativeMap();
    //                 result.putString("uri", mImageFilePath);
    //                 mCallback.invoke(null, result);
    //             } else {
    //                 mCallback.invoke("Image capture error");
    //             }
    //             mCallback = null;
    //         }
    //     }
    // };


    @Override
    public void onNewIntent(Intent intent){
//        Do nothing...
    }
}
