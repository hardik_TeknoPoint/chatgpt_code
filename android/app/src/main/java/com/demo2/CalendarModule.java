//package demo2; // replace com.your-app-name with your app’s name
package com.demo2; // replace com.your-app-name with your app’s name

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
// import android.telephony.TelephonyManager;
// import android.content.Context;

public class CalendarModule extends ReactContextBaseJavaModule {
    public static String eventId = "eventId";

    CalendarModule(ReactApplicationContext context) {
        super(context);
    }
//    @ReactMethod(isBlockingSynchronousMethod = true);
    @ReactMethod
    public void createCalendarEvent(String name, String location,Callback callback) {
        android.util.Log.d("logged", "createCalendarEvent: ");
        // TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        // String phoneNumber = telephonyManager.getLine1Number();
        // callback.invoke("i am from java."+phoneNumber);
        callback.invoke("i am from java.");
    }

    @ReactMethod
    public void promiseFunction(String name, String location, Promise promise) {
       try {
           throw new Exception("error from  promise..");
//           promise.resolve("success");
       }catch (Exception e){
           promise.reject("error ocuored",e);
       }
    }

   @ReactMethod
   public void getFullName(String fname, String lname, Callback callBack,Callback errorCallBack){
    try {
        MyPromiseDemo myPromiseDemo = new MyPromiseDemo(fname,lname);
           String fUllName = myPromiseDemo.makeFullName();
        callBack.invoke(fUllName);
    } catch (Exception e) {
        errorCallBack.invoke(e);
    }
   }

    @Override
    public String getName(){
        return  "CalendarModule";
    }
}