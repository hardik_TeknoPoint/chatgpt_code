//package com.demo2;
//
//
//import androidx.annotation.NonNull;
//
//import com.facebook.drawee.backends.pipeline.Fresco;
//import com.facebook.react.bridge.ReactApplicationContext;
//import com.facebook.react.uimanager.SimpleViewManager;
//import com.facebook.react.uimanager.ThemedReactContext;
//import com.facebook.react.views.image.ReactImageView;
//
//public class MyCustomView extends SimpleViewManager<ReactImageView> {
//
//    public static final String REACT_CLASS = "RCTImageView";
//    ReactApplicationContext mCallerContext;
//
//    public MyCustomView(ReactApplicationContext reactContext) {
//        mCallerContext = reactContext;
//    }
//
//    @Override
//    public String getName() {
//        return REACT_CLASS;
//    }
//
//    @NonNull
//    @Override
//    protected ReactImageView createViewInstance(@NonNull ThemedReactContext themedReactContext) {
//        return new MyCustomView(mCallerContext, Fresco.newDraweeControllerBuilder(), null, mCallerContext);;
//    }
//}