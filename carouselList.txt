Documentation for flatlist carousel

This is carousel  have created by using  flatlist element from react-native default elements.
This carousel usses some props as per follows.

E.g.

DATA = which take an array of item which user have to render in list view.
ViewAbilityConfig = we pass useref of which provided by react.
handleScroll = this props give handle scroll availability. To user by providing default eventData from flatlist.
Item_width = here we have to provide item with which we have to render by default item width is 90% of full screen width.
Item_margin = here we have to provide margin to our item by default margin of each item is 0.
carousel_width = carousel width is what your carousel width to display carousel in screen by default carousel width is ( 80% screen width + item_margin * 2 ).
defaultCenter =  this is going to be boolean value if passing true then it renders item in center of carousel or else you can define as you wish.
renderItemFunc = this is going to be function which going to render you item in list. In this function we provide item, index by default params.